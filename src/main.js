var myshop = new Shop('Alice\'s Shop')


myshop.add(123, "Learn TypeScript", 16.99)
myshop.add(234, "Learn NodeJS", 25.99, true)

// ====

var cart = new ShoppingCart()

var product = myshop.getProduct(123)
cart.add(product)


console.log('Total = ' + cart.getTotal())

// ====

function ProductList(elem, products, rowClickCallback) {
  
  $(elem).on('click', 'tr', function(event){
    rowClickCallback($(this).data('id'))
  })
  this.products = products
  
  this.setData = function(products){
    this.products = products
  }

  this.render =function(){
    $(elem).find('.products-list').empty()

    this.products.forEach(function (product, index) {
      // Create Element
      var productElem = $('<tr data-id="'+product.id+'">' +
        '<td>' + product.id + '</td>' +
        '<td>' + product.name + '</td>' +
        '<td>' + product.price + '</td>' +
        '<td>' + (new Date(product.updated).toLocaleDateString()) + '</td>' +
        '</tr>')
  
      // $(productElem).css('color','red')
      // Append to list
      $(elem).find('.products-list').append(productElem)
    })
  }
}
var productsView = new ProductList('.products-catalogue', myshop.getAll(), function(id){
  cart.add( myshop.getProduct(id) )
  cartView.render()
})
productsView.render()

var cartView =  new ProductList('.products-cart', cart.items, function(id){
  cart.remove(id)
  cartView.setData(cart.items)
  cartView.render()
})
cartView.render()


setInterval(function (){
  d = new Date()
  $('h1').text( d.toLocaleTimeString() )
},1000)

$('.products-filter').on('keyup', function(event){
  var results = myshop.searchByName(event.target.value)
  productsView.setData(results)
  productsView.render()
})