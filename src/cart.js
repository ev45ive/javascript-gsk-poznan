function ShoppingCart() {
  this.items = []
  this.total = 0
}

ShoppingCart.prototype.add = function (product) {
  if ('undefined' == typeof product ||
    'undefined' == typeof product.id ||
    'undefined' == typeof product.price) {

    throw 'Cant add Product - missing Id or Price'
  }
  this.items.push(product)
}


ShoppingCart.prototype.remove = function (id) {
  this.items = this.items.filter(function (item) {
    return item.id == id
  })
}

ShoppingCart.prototype.getTotal = function () {
  this.total = 0
  // var self = this

  this.items.forEach(function (item) {
    this.total += item.price

  }.bind(this))

  return this.total
}