function Shop(shopName) {

  var products = []
  this.name = shopName

  this.getAll = function () {
    return products
  }

  this.getProduct = function (id) {
    return products.filter(function (item) {
      return item.id == id
    }).pop()
  }

  this.searchByName = function (name) {
    return products.filter(function (item) {
      return item.name.toLowerCase().includes(name.toLowerCase())
    })
  }

  this.add = function (id, name, price, promo) {

    if (isNaN(price) || !isFinite(price) || price < 0) {
      throw 'Invalid Price'
    }

    products.push({
      id: id,
      name: name,
      price: price,
      promo: !!promo,
      updated: (new Date()).toUTCString()
    })
  }

}