class Person{

  constructor(name){
    this.name = name
  }

  sayHello(){
    return this.name
  }
  
}

class Employee extends Person{
  
  constructor(name,salary){
    super()
	this.salary = salary
  }
 
  work(){
    return 'I am on vacation!'
  }
}