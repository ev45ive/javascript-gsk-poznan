function Person(name){
	this._name = name 
}
Object.assign(Person.prototype,{
	sayHello: function(){
        return 'Hi, I am ' + this._name
    },
	toString: function(){ return 'Howdy!' }
})

function Employee(name, salary){
	Person.apply(this, arguments )
	this._salary = salary
}
Employee.prototype = Object.create(Person.prototype)
Employee.prototype.work = function(){
	return 'I want my ' + this._salary
}

var alice = new Person('Alice')
var bob = new Employee('Bob', 2400)
